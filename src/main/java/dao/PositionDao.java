package dao;

import model.Position;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static util.RandomGenerator.generateAvailablePositions;

@Repository
public class PositionDao {
    private final Map<String, List<Position>> availablePositions = generateAvailablePositions(20);

    public Map<String, List<Position>> getAllAvailablePositions() {
        return availablePositions;
    }

    public List<Position> getAvailablePositionsIn(String skill) {
        return availablePositions.getOrDefault(skill, new ArrayList<>());
    }

}
