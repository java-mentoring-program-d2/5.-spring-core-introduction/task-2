package dao;

import model.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static util.RandomGenerator.generateEmployees;

@Repository
public class EmployeeDao {
    private static final Map<String, List<Employee>> employees = generateEmployees(5);

    public Map<String, List<Employee>> getAllEmployees() {
        return employees;
    }

    public List<Employee> getEmployeesAt(String skill) {
        return employees.getOrDefault(skill, new ArrayList<>());
    }

    public boolean remove(Employee employee) {
        List<Employee> empls = employees.get(employee.getSkill());
        if (empls == null || empls.isEmpty()) {
            return false;
        }
        return empls.remove(employee);
    }

    public void add(Employee employee) {
        String skill = employee.getSkill();
        List<Employee> empls = employees.getOrDefault(skill, new ArrayList<>());
        empls.add(employee);
        if (empls.size() == 1) {
            employees.put(skill, empls);
        }
    }
}
