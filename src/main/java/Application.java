import model.Employee;
import model.Position;
import model.Salary;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import service.EmployeeService;
import service.PositionService;
import service.SalaryService;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static util.RandomGenerator.generateCandidates;
import static util.RandomGenerator.getRandom;
import static util.RandomGenerator.getSkills;

public class Application {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext("service", "dao");
        EmployeeService employeeService = context.getBean(EmployeeService.class);
        PositionService positionService = context.getBean(PositionService.class);
        SalaryService salaryService = context.getBean(SalaryService.class);
        Random random = getRandom();
        int weeksPass = 1;
        while (weeksPass < 113) {
            Map<String, List<Employee>> employed = employeeService.getAllEmployees();
            Map<String, List<Position>> availablePositions = positionService.getAvailablePositions();
            long employeesAmount = employed.entrySet().stream()
                    .flatMap(entry -> entry.getValue().stream())
                    .count();
            long positionsAmount = availablePositions.entrySet().stream()
                    .flatMap(entry -> entry.getValue().stream())
                    .count();
            System.out.println("Weak " + weeksPass + " starts. Employed: " + employeesAmount);
            System.out.println("Available positions : " + positionsAmount);

            List<Salary> salaries = employed.entrySet().stream()
                    .flatMap(entry -> entry.getValue().stream())
                    .map(Employee::getPosition)
                    .map(Position::getSalary)
                    .collect(Collectors.toList());

            double averageSalaryThisWeek = salaries.stream()
                    .mapToInt(Salary::getSalary)
                    .average()
                    .orElse(0.0);
            System.out.println("Average salary through the whole company is: " + averageSalaryThisWeek);
            salaryService.updateSalariesWithChanges(salaries);


            if (employeesAmount > 15) {
                int amountOfFiredThisWeek = random.nextInt(2) + 1;
                int count = 0;
                List<String> skills = getSkills();
                for (int i = 0; i < amountOfFiredThisWeek; i++) {
                    Employee fired = employeeService.fireRandomlyAt(skills.get(random.nextInt(skills.size())));
                    if (fired != null) {
                        count++;
                    }
                }
                System.out.println(count + " employees are fired this week");
            }

            List<Employee> listOfCandidates = generateCandidates(random.nextInt(9) + 1);
            AtomicInteger amountNewcomers = new AtomicInteger();
            listOfCandidates.forEach(employee -> {
                List<Position> avPositions = positionService.findAvailableBy(employee.getSkill());
                if (!avPositions.isEmpty()) {
                    for (Position position : avPositions) {
                        if (position.getRequiredSkill() >= employee.getSkillLevel() &&
                                employee.getExpectedSalaryMinimum() <= position.getSalary().getSalary()) {
                            employee.setPosition(position);
                            positionService.removePositionFromAvailable(position);
                            employeeService.addEmployee(employee);
                            amountNewcomers.getAndIncrement();
                            break;
                        }
                    }
                }
            });
            System.out.println(amountNewcomers.get() + " new employees this week");
            weeksPass++;
            System.out.println("\n<====================>\n");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
