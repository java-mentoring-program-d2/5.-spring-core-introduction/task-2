package service;

import dao.PositionDao;
import model.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

@Service
public class PositionService {
    private PositionDao positionDao;

    @Autowired
    public PositionService(PositionDao positionDao) {
        this.positionDao = positionDao;
    }

    public PositionService() {
    }


    public Map<String, List<Position>> getAvailablePositions() {
        return positionDao.getAllAvailablePositions();
    }

    public boolean addPositionToAvailable(Position position) {
        String skill = position.getName();
        position.setEmployee(null);
        Map<String, List<Position>> availablePositions = getAvailablePositions();
        if (!availablePositions.containsKey(skill)) {
            availablePositions.put(skill, new ArrayList<>());
        }
        return availablePositions.get(skill).add(position);
    }

    public Position removePositionFromAvailable(Position position) {
        if (getAvailablePositions().get(position.getName()).remove(position)) {
            return position;
        }
        throw new NoSuchElementException("Can't remove position from available positions. There is no element with id: "
                + position.getId());
    }


    public List<Position> findAvailableBy(String name) {
        return getAvailablePositions().entrySet().stream()
                .filter(p -> p.getKey().equals(name))
                .map(Map.Entry::getValue)
                .findFirst()
                .orElse(new ArrayList<>());
    }


    public Position findAvailableById(int id) {
        return getAvailablePositions().entrySet().stream()
                .flatMap(entry -> entry.getValue().stream())
                .filter(position -> position.getId() == id)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Position with id: " + id + " wasn't found at available"));
    }


    public void updatePosition(Position position) {
        int id = position.getId();
        final Position positionToUpdate = findAvailableById(id);
        changePosition(position, positionToUpdate);
    }

    private void changePosition(Position newOne, Position oldOne) {
        oldOne.setEmployee(newOne.getEmployee());
        oldOne.setName(newOne.getName());
        oldOne.setRequiredSkill(newOne.getRequiredSkill());
        oldOne.setSalary(newOne.getSalary());
    }

}
