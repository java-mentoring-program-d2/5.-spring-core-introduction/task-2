package service;

import dao.EmployeeDao;
import model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Service
public class EmployeeService {
    private PositionService positionService;
    private SalaryService salaryService;
    private EmployeeDao employeeDao;

    private String testString;

    public String getTestString() {
        return testString;
    }

    public void setTestString(String testString) {
        this.testString = testString;
    }

    public EmployeeService() {
    }

    @Autowired
    public EmployeeService(PositionService positionService,
                           SalaryService salaryService,
                           EmployeeDao employeeDao) {
        this.positionService = positionService;
        this.salaryService = salaryService;
        this.employeeDao = employeeDao;
    }

    public Map<String, List<Employee>> getAllEmployees() {
        return employeeDao.getAllEmployees();
    }

    public List<Employee> getEmployeesAt(String skill) {
        return employeeDao.getEmployeesAt(skill);
    }

    public Employee fireRandomlyAt(String position) {
        if (isPositionEmpty(position)) {
            return null;
        }
        final List<Employee> emps = employeeDao.getEmployeesAt(position);
        final Employee employeeToFire = emps.get(new Random().nextInt(emps.size()));
        employeeDao.remove(employeeToFire);
        positionService.addPositionToAvailable(employeeToFire.getPosition());
        return employeeToFire;
    }

    public boolean fireTheWorstAt(String position) {
        if (isPositionEmpty(position)) {
            return false;
        }
        final List<Employee> emps = employeeDao.getEmployeesAt(position);
        final Employee employeeToFire = emps.stream().min(Comparator.comparing(Employee::getSkillLevel)).get();
        employeeDao.remove(employeeToFire);
        positionService.addPositionToAvailable(employeeToFire.getPosition());
        return emps.remove(employeeToFire);
    }


    public void addEmployee(Employee employee) {
        employeeDao.add(employee);
    }

    private boolean isPositionEmpty(String position) {
        final List<Employee> emps = employeeDao.getAllEmployees().get(position);
        return (emps == null || emps.size() == 0);
    }

    @Override
    public String toString() {
        return "EmployeeService{" +
                ", \npositionService=" + positionService +
                ", \nsalaryService=" + salaryService +
                ", \ntestString='" + testString + '\'' +
                '}';
    }
}