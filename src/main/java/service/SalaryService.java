package service;

import model.Salary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class SalaryService {
    public static Salary updateSalaryWithChanges(Salary salary) {
        final Random random = new Random();
        int change = random.nextInt(30) + 1;
        if (random.nextInt(2) == 0) {
            change = -1 * change;
        }
        salary.setSalary(salary.getSalary() + change);
        return salary;
    }

    public List<Salary> updateSalariesWithChanges(List<Salary> salaries) {
        salaries.forEach(SalaryService::updateSalaryWithChanges);
        return salaries;
    }
}
