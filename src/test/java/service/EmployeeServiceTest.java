package service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.JUnit38ClassRunner;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

public class EmployeeServiceTest {
    private EmployeeService employeeService;

    public EmployeeServiceTest() {
    }

    @Before
    public void setUp() throws Exception {
        ApplicationContext context = new AnnotationConfigApplicationContext("service", "dao");
        employeeService = context.getBean(EmployeeService.class);
    }

    @Test
    public void fireRandomlyAt() {
        Assert.assertNotNull(employeeService);
        System.out.println("Test application context started!");
    }

}